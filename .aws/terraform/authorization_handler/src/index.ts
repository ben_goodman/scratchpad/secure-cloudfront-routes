import { type Handler, type CloudFrontRequestEvent, type CloudFrontRequest } from 'aws-lambda'
import { CognitoJwtVerifier } from 'aws-jwt-verify'

// extracts the session-auth cookie from the request headers
const extractToken = (request: CloudFrontRequest) => {
    const requestHeaders = request.headers
    const cookies = requestHeaders.cookie || []
    const COOKIE_NAME = 'session-auth'

    return cookies.map((cookie) => {
        const [name, value] = cookie.value.split('=')
        if (name === COOKIE_NAME) {
            return value
        }
    })[0]
}

// redirects the user to the app's login page
const handleUnauthorizedRequest = {
    status: '302',
    statusDescription: 'Found',
    headers: {
        location: [{
            key: 'Location',
            value: '/login',
        }],
    },
}

// will examine each request and determine if it is a secure path
// if it is, it will verify the JWT token included in the request
// if the token is valid, it will allow the request to pass through to the origin
// if the token is invalid, it will redirect the user to the login page
// if the request is not a secure path, it will pass the request to the origin
export const handler: Handler<CloudFrontRequestEvent> = async (event) => {
    const request = event.Records[0].cf.request;
    // a secure path is one that matches /secure/*
    const isSecurePath = request.uri.split('/')[1] === 'secure'

    // a secure path will require authentication
    if (isSecurePath) {
        const jwt = extractToken(request)

        if (jwt) {
            // Verifier that expects valid access tokens:
            const verifier = CognitoJwtVerifier.create({
                userPoolId: process.env.USER_POOL_ID!,
                clientId: process.env.APP_CLIENT_ID!,
                tokenUse: "access",
            })

            try {
                await verifier.verify(jwt)
                return request
            } catch {
                return handleUnauthorizedRequest
            }
        } else {
            return handleUnauthorizedRequest
        }

    } else {
        //non-secure path (passthrough)
        return request
    }
}