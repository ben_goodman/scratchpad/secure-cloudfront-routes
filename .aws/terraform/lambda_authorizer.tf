# Use local-exec to build the lambda function during the terraform apply.
# This is required since the lambda function requires the cognito user
# pool id and app client id.  However, environment variables are not
# supported by Lambda@Edge so they are baked into the source code.
resource "null_resource" "build_handler_function" {
    provisioner "local-exec" {
        working_dir = "${path.module}/authorization_handler"
        # uses a makefile to abstract the build process
        command     = "make all"
        environment = {
            USER_POOL_ID  = aws_cognito_user_pool.default_pool.id
            APP_CLIENT_ID = aws_cognito_user_pool_client.default_client.id
        }
    }
    triggers = {
        always_run = "${timestamp()}"
    }
}

# Without this data source, the archive_file will fail with the following error:
# error creating archive: error archiving file: could not archive missing file
data "null_data_source" "wait_for_lambda_exporter" {
  inputs = {
    # This ensures that this data resource will not be evaluated until
    # after the null_resource has been created.
    lambda_exporter_id = "${null_resource.build_handler_function.id}"

    # This value gives us something to implicitly depend on
    # in the archive_file below.
    source_file = "${path.module}/authorization_handler/dist/index.js"
  }
}

# This data source is used to create a zip file from the lambda function
data "archive_file" "lambda_payload_viewer_request_authorizer" {
    depends_on = [ data.null_data_source.wait_for_lambda_exporter ]
    type             = "zip"
    source_file      = data.null_data_source.wait_for_lambda_exporter.outputs.source_file
    output_file_mode = "0666"
    output_path      = "${path.module}/authorization_handler/dist/index.js.zip"
}

# This module creates a lambda function that is used to authorize requests
module "viewer_request_authorizer" {
    source = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "1.3.0"
    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = data.archive_file.lambda_payload_viewer_request_authorizer
    function_name    = "jwt-auth-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 128
}
