output "user_pool_client_id" {
    value = aws_cognito_user_pool_client.default_client.id
}

output "cloudfront_default_domain" {
    value = module.cloudfront.cloudfront_default_domain
}

output "cloudfront_id" {
    value = module.cloudfront.cloudfront_id
}

output "default_s3_bucket_name" {
    value = module.cloudfront.bucket_name
}