module "cloudfront" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "2.0.0"

    org          = var.resource_namespace
    project_name = var.project_name
    use_cloudfront_default_certificate = true

    default_cache_policy_id = aws_cloudfront_cache_policy.default.id
    default_response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers.id

    default_lambda_function_associations = {
        "viewer-request" = {
            lambda_arn = module.viewer_request_authorizer.qualified_arn
        }
    }
}
