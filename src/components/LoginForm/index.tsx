import { AuthFlowType, CognitoIdentityProviderClient, InitiateAuthCommand } from '@aws-sdk/client-cognito-identity-provider'
import React, { useState } from 'react'
import { useCookies } from 'react-cookie'
import styled from 'styled-components'

const SESSION_AUTH_COOKIE = 'session-auth'

const StyledLogin = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 600px;
    border: 1px solid black;
    padding: 20px;
    margin: 20px;
    border-radius: 10px;

    label, input, button {
        margin-bottom: 10px;
    }

    input, button {
        padding: 5px;
    }
`

export interface LoginProps {
    onAuthenticated?: () => void
}

export const LoginForm  = ({
    onAuthenticated = () => {},
}: LoginProps) => {
    const [username, setUsername] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [, setCookie] = useCookies([SESSION_AUTH_COOKIE])
    const [disabled, setDisabled] = React.useState<boolean>(false)
    const [error, setError] = React.useState<string|undefined>(undefined)



    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value, name } = e.target

        if (name === 'username') {
            setUsername(value)
            return
        }

        if (name === 'password') {
            setPassword(value)
            return
        }

        throw new Error('Unknown input source')
    }


    const handleAuthSubmit = async () => {
        setDisabled(true)

        const client = new CognitoIdentityProviderClient({
            region: 'us-east-1'
        })

        const command = new InitiateAuthCommand({
            AuthFlow: AuthFlowType.USER_PASSWORD_AUTH,
            AuthParameters: {
                USERNAME: username,
                PASSWORD: password,
            },
            ClientId: process.env.USER_POOL_CLIENT_ID,
        })

        try {
            const data = await client.send(command)
            setCookie(SESSION_AUTH_COOKIE, data.AuthenticationResult?.AccessToken)
            onAuthenticated()
        } catch (error) {
            if ((error as Error).name === 'NotAuthorizedException') {
                setError('Incorrect username or password')
                setTimeout(() => {
                    setError(undefined)
                }, 5000)
            }
        } finally {
            setDisabled(false)
        }
    }

    return (
        <StyledLogin>
            <label htmlFor="username">Username:</label>
            <input type='text' id='username' name='username' value={username} onChange={handleInput}/>

            <label htmlFor="password">Password:</label>
            <input type='password' id='password' name='password' value={password} onChange={handleInput}/>

            <button disabled={disabled} onClick={handleAuthSubmit}>Submit</button>
            {error && <p>{error}</p>}
        </StyledLogin>
    )
}