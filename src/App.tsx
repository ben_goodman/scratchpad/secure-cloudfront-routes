import React from 'react'
import { RouterProvider } from 'react-router-dom'
import { router } from './views/router'
import { CookiesProvider } from 'react-cookie'


export const App = () => {
    return (
        <CookiesProvider>
            <RouterProvider  router={router}/>
        </CookiesProvider>
    )
}