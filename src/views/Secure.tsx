import React from 'react'
import { useNavigate } from 'react-router-dom'

export const Component = () => {
    const navigate = useNavigate()

    const navigateLogOut = () => {
        navigate('/log-out')
    }

    return (
        <>
            <p>Secure</p>
            <button onClick={navigateLogOut} >Log Out</button>
        </>
    )
}

Component.displayName = 'Secure'
