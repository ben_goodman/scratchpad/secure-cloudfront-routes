import React from 'react'
import { useNavigate } from 'react-router-dom'
import { LoginForm } from 'src/components/LoginForm'


export const Component = () => {
    const navigate = useNavigate()

    const redirectToSecure = () => {
        navigate('/secure')
    }

    return (
        <>
            <h1>Login</h1>
            <LoginForm onAuthenticated={redirectToSecure} />
        </>
    )
}

Component.displayName = 'Login'