import { CognitoIdentityProvider } from '@aws-sdk/client-cognito-identity-provider'
import React, { useEffect } from 'react'
import { useCookies } from 'react-cookie'


const SESSION_AUTH_COOKIE = 'session-auth'


export const Component = () => {
    const [cookie, setCookie] = useCookies([SESSION_AUTH_COOKIE])

    const handleLogOut = async () => {

        const client = new CognitoIdentityProvider({
            region: 'us-east-1'
        })

        try {
            const AccessToken = cookie[SESSION_AUTH_COOKIE] as string
            client.globalSignOut({AccessToken})
        } catch (err) {
            console.error(err)
        } finally {
            setCookie(SESSION_AUTH_COOKIE, undefined)
        }
    }

    useEffect(() => {
        handleLogOut()
    }, [])

    return (
        <>
            <p>You have been logged out</p>
        </>
    )
}

Component.displayName = 'LogOut'