import React from 'react'
import { Outlet, Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom'


const Main = () => {
    return <>
        <Outlet />
    </>
}

export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path='/' element={<Main />}>
            <Route index lazy={() => import('./Home')} />
            <Route
                path="/login"
                lazy={() => import('./LogIn')}
            />
            <Route
                path="/secure"
                lazy={() => import('./Secure')}
            />
            <Route
                path="/log-out"
                lazy={() => import('./LogOut')}
            />
            <Route
                path="*"
                lazy={() => import('./Error')}
            />
        </Route>
    )
)